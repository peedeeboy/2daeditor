/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;

import java.io.*;
import java.nio.*;
import java.util.*;

/**
 *
 * @author pete
 */
public class TwoDABroker {

    Object[][] data;
    
    public TwoDABroker() {
        
        
    }
    
    
    public TwoDA Open(File filein) {
        
        /* Declare Variable and Constants */
        final int NEWLINE = 10;                                                 // Constant for a ASCILI newline char
        final int NULL = 0;                                                     // Constant for an ASCII null
        final int TAB = 9;                                                      // Constant for an ASCII TAB
        final int FILESIZE = (int) filein.length();                             // Constant to hold the filesize (bytes)
        
        String header = "";                                                     // Variable to store the header name
        int rowCount= 0;                                                        // Variable to store the row count
        int columnCount = 0;                                                    // Variable to store the column count
        int cellCount = 0;                                                      // Variable to store the cell count
        List<String> columnNames = new ArrayList<String>();                     // List to store the column names
        List<String> rowNames = new ArrayList<String>();                        // List to store the row names
        int[] offsets;                                                          // Array to hold offsets
        //Object[][] data;                                                        // Two dimensional array to hold data
        
        int r = 0;                                                              // Variable to byte read in decimal
        char ch = 0;                                                            // Variable used to covert byte to ASCII
        
        
        /* Open File */
        try {            
            FileInputStream fs = new FileInputStream(filein);                   // Open a new filestream
            DataInputStream ds = new DataInputStream(new BufferedInputStream(fs));    // Open a new datastream

        
            /* Get Header */
            r = ds.readByte();                                                  // Read first byte
            while(r != NEWLINE) {                                               // Read bytes until we hit a newline char
                ch = (char) r;                                                  // Convert decimal to char
                header += ch;                                                   // Add char to header
                r = ds.readByte();                                              // Read next byte
            }
        
            
            /* Get Column Names */
            r = ds.readByte();                                                  // Read next byte
            while(r != NULL) {                                                  // Read bytes until we hit a null char
                
                String column = "";                                             // Variable to 'gather' column name
                
                while(r != TAB) {                                               // Read bytes until we hit a tab
                    ch = (char) r;                                              // Convert decimal to char
                    column += ch;                                               // Add char to column name
                    r = ds.readByte();                                          // Read next byte
                }
                r = ds.readByte();                                              // Read next byte
                columnCount++;                                                  // Increment column count
                columnNames.add(column);                                        // Add column to list of columnNames
                
            }
            
            
            /* Get rowcount */
            byte[] byteArray = new byte[4];                                     // Create a byte array of four bytes (to hold 32 to bit int)
            for(int i = 0; i < 4; i++) {                                        // Read next four bytes
                byteArray[i] = ds.readByte();                                   // into the byte array
            }
            
            ByteBuffer buffer = ByteBuffer.wrap(byteArray);                     // Stuff the byte array into a new Byte Buffer
            buffer.order(ByteOrder.LITTLE_ENDIAN);                              // Convert the byte order of the int from BIG_ENDIAN to LITTLE_ENDIAN
            rowCount = buffer.getInt();                                         // Get rowCount
            
            
            /* Get rownames */
            for(int i = 0; i < rowCount; i++) {                                 // Iterate once for each row
                
                String row = "";                                                // Variable to 'gather' row name
                r = ds.readByte();                                              // Read next byte
                while(r != TAB) {                                               // Read bytes until we hit a TAB char
                    ch = (char) r;                                              // Convert decimal to char
                    row += ch;                                                  // Add char to row name
                    r = ds.readByte();                                          // Read next byre
                }
                
                rowNames.add(row);                                              // Add row name to list
                
            }

            
            /* Get offsets */
            cellCount = rowCount * columnCount;                                 // Calculate number of offsets
            offsets = new int[cellCount];                                       // Create array to hold offsets

            for(int i = 0; i < cellCount; i++) {                                // Iterate once for each offset

                byte[] offsetByteArray = new byte[2];                           // Create new two byte array to store 16 bit number

                for(int j = 0; j < 2; j++) {                                    // Get next two bytes 
                    offsetByteArray[j] = ds.readByte();                         // and put them in the array
                }

                ByteBuffer offBuf = ByteBuffer.wrap(offsetByteArray);           // Stuff the byte array a new byte buffer
                offBuf.order(ByteOrder.LITTLE_ENDIAN);                          // Convert the byte order to LITTE_ENDIAN
                int offset = offBuf.getChar();                                  // Get the 16 bit number (Pop fact: char is a 16 bit unsigned number datatype!)

                offsets[i] = offset;                                            // Put offset in array

            }
                
                
            /* Skip two unused bytes */
            ds.skipBytes(2);                                                    // TODO: check these bytes are both NULL
                
                
            /* Get Data */
            this.data = new Object[rowCount][columnCount];                           // Create two dimensional area to hold data
            ds.mark(FILESIZE);                                                  // Mark start of data area
            
            int sent = 0;
                
            for(int row = 0; row < rowCount; row++) {                       // Outer loop: Iterate through data rows

                for(int col = 0; col < columnCount; col++) {                // Inner loop: Iterate through columns

                    String value = "";                                      // Variable to 'gather' data value

                    ds.skipBytes(offsets[sent]);                                      // Skip pointer to the offset
                    r = ds.read();                                          // Read next byte

                    while(r != NULL && r != -1) {                           // Read until we hit a null char
                        ch = (char) r;                                      // Convert decimal to ASCII char
                        value += ch;                                        // Add char to value string
                        
                        r = ds.readByte();                                  // Read next byte
                    }
                    
                    sent++;
                    this.data[row][col] = value;                                 // Add value to 2DA
                    ds.reset();    
                    }
                

                }
            
            
               

            ds.close();
            fs.close();
        
            
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        
        
        TwoDA twoda = new TwoDA(header, columnNames, rowNames, rowCount,
                columnCount, this.data);
            return twoda ;
        }

    }

