/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;


import javax.swing.*;          
import java.awt.*;
import java.awt.event.*;
import javax.swing.plaf.metal.*;


/**
 *
 * @author pete
 */
public class Editor {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       /* Set System Look and Feel */ 
       try { 
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
       } 
       catch (Exception e) {
            e.printStackTrace();
        }
        
        /* Launch application */
        UI window = new UI();                                                   // Create a new window
        window.pack();                                                          // Pack the window
        window.setVisible(true);                                                // Make window visible
        
    }
}
