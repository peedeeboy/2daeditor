/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;

/**
 *
 * @author pete
 */
    import java.awt.*;  
    import javax.swing.*;  
       
    public class SizeTest  
    {  
        JTable table;  
       
        public SizeTest()  
        {  
            table = getTable();  
            Dimension d = table.getPreferredSize();  
            table.setPreferredScrollableViewportSize(d);  
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  
        }  
       
        private JTable getTable()  
        {  
            String[] headers = { "column 1", "column 2", "column 3", "column 4" };  
            int rows = 18, cols = 4;  
            Object[][] data = new Object[rows][cols];  
            for(int row = 0; row < rows; row++)  
                for(int col = 0; col < cols; col++)  
                    data[row][col] = "item " + (row * cols + col + 1);  
            return new JTable(data, headers);  
        }  
       
        public static void main(String[] args)  
        {  
            SizeTest test = new SizeTest();  
            JFrame f = new JFrame();  
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
            f.getContentPane().add(new JScrollPane(test.table));  
            f.pack();  
            f.setLocation(200,200);  
            f.setVisible(true);  
        }  
    }  
