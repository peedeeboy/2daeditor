/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;

import java.io.*;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author pete
 */
public class Test {
    
    
    
    public static void main(String[] args) {
        
        int columnCount = 0;
        String header = "";
        
        final int NEWLINE = 0x0A;
        
        // Set file path
        File filename = new File("C:\\Temp\\SWKotOR2\\override\\modulesave.2da");
        
        int filesize = (int) filename.length();
        
        /* Parse Steam */
        try {
            FileInputStream file = new FileInputStream(filename);
            DataInputStream stream = new DataInputStream(new BufferedInputStream(file));
            
            
            /* Print Header */
            int r;
            char ch;
            
            r = stream.readByte();
            while(r != 10) {
                ch = (char) r;
                header += ch;
                r = stream.readByte();
            }
            
            System.out.println(header);
            
            
            /* Print Column Names */
            r = stream.readByte();
            while(r != 0) {
                
                String column = "";
                
                while(r != 9) {
                    ch = (char) r;
                    column += ch;
                    r = stream.readByte();
                }
                r = stream.readByte();
                columnCount++;
                System.out.println(column);
                
            }
            
            /* Get rowcount */
            
            byte[] byteArray = new byte[4];
            for(int i = 0; i < 4; i++) {
                byteArray[i] = stream.readByte();
            }
            
            ByteBuffer buffer = ByteBuffer.wrap(byteArray);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int rowcount = buffer.getInt();
            
            
            
            System.out.println(rowcount);
            
            
            /* Get rownames */
            
            for(int i = 0; i < rowcount; i++) {
                
                String row = "";
                r = stream.readByte();
                while(r != 9) {
                    ch = (char) r;
                    row += ch;
                    r = stream.readByte();
                }
                
                System.out.println(row);
            }
            
            
            /* Get offsets */         
            
            Object[][] twoDA = new Object[rowcount][columnCount];
            
            for(int row = 0; row < rowcount; row++) {
                
                for(int col = 0; col < columnCount; col++) {
                byte[] offsetArray = new byte[2];

                for(int j = 0; j < 2; j++) {
                    offsetArray[j] = stream.readByte();
                }

                ByteBuffer offBuf = ByteBuffer.wrap(offsetArray);
                offBuf.order(ByteOrder.LITTLE_ENDIAN);
                int offset = offBuf.getChar();

                System.out.print(offset + " ");
                twoDA[row][col] = offset;
                
                }
                
                System.out.println();
                
            }
            
            // Skip two unused bytes
            stream.skipBytes(2);                                                
          
            // Mark start of data area             
            stream.mark(filesize);
            
            for(Object[] arr2: twoDA) {
                
                for(Object val: arr2) {
                
                    stream.skipBytes((int) val);
                    
                    r = stream.read();
                    
                    if(r == 0) {
                        System.out.print("****");
                    }
                    else {
                        while(r != 0) {
                            ch = (char) r;
                            System.out.print(ch);
                            r = stream.readByte();
                        }
                    }
                    
                    System.out.print(" ");
                    stream.reset();
                    
                }
                
                System.out.println();
                
            }

            
            stream.close();
            file.close();
            
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        
        
    }
        
}
