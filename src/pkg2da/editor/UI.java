/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author pete
 */
public class UI extends JFrame implements ActionListener  {
    
    /* Properties */
    private JMenuBar menuBar = new JMenuBar();
    private JMenu fileMenu = new JMenu("File");
    private JMenuItem selectOpen = new JMenuItem("Open");
    private JMenuItem selectExit = new JMenuItem("Exit");
    private File chosenFile;
    
    public UI() {
        
        /* Set up window */
        this.setTitle("2da Reader");                                            // Set Window Title
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                    // Close the application when UI is closed
        this.setLayout(new BorderLayout());                                       // Using a flow layout
        this.setMinimumSize(new Dimension(500, 500));                           // Make the window a 500 x 500 square
        this.setLocation(100, 100);                                             // Position the window on the desktop
       
        /* Set up MenuBar */
        menuBar.add(fileMenu);                                                  // Add Menu -> File
        fileMenu.add(selectOpen);                                               // Add Menu -> File -> Open
        fileMenu.add(selectExit);                                               // Add Menu -> File -> Exit
        
        /* Add controls to window */
        this.setJMenuBar(menuBar);                                              // Add menubar to window
        
        /* Add ActionListeners */
        selectOpen.addActionListener(this);
        selectExit.addActionListener(this);

        }
    
    
    public void actionPerformed(ActionEvent e) {
        
        /* File -> Open */
        if(e.getSource() == selectOpen) {
            
            /* Create a new file chooser */
            JFileChooser chooser = new JFileChooser();
            chooser.showDialog(this, "Select File");
            chosenFile = chooser.getSelectedFile();
            System.out.println(chosenFile);
            TwoDABroker brk = new TwoDABroker();
            
            TwoDA tda = brk.Open(chosenFile);
            
                        
            Object[] columnNames = tda.getColumnNames().toArray();
            Object[][] data = tda.getData();
            
            // Create the table
            JTable table = new JTable(data, columnNames);
            table.setFillsViewportHeight(true);
            

            // Create the row header tabel
            JList<String> rowHeaders = new JList(tda.getRowNames().toArray());

            
            //Create the scroll pane and add the table to it.
            JScrollPane scrollPane = new JScrollPane(table);
            scrollPane.setRowHeaderView(rowHeaders);


            //Add the scroll pane to this panel.
            this.add(scrollPane, BorderLayout.CENTER);
            scrollPane.repaint();
            super.repaint();
            
            
        }
        
        /* File -> Exit */
        if(e.getSource() == selectExit) {
            
            /* Exit the application */
            System.exit(0);
            
        }
        
        
        
    }
}