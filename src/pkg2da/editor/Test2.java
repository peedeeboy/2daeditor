/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;


import java.io.*;
import java.nio.*;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.*;


/**
 *
 * @author pete
 */
public class Test2 {
    
    public static void main(String args[]) {
        
        // Set file path
        Path path = Paths.get("C:\\Temp\\SWKotOR2\\override\\modulesave.2da");
        
        // Open file
        try{
            
            SeekableByteChannel bc = Files.newByteChannel(path, StandardOpenOption.READ);
            
            System.out.println("Length: " + bc.size());
            
            // Set cursor postion
            long pos = 0;
            bc.position(pos);
            
            // Output length to console
            System.out.println("Position: " + bc.position());
            
            // Get Header
            ByteBuffer buf = ByteBuffer.allocate(8);
            bc.read(buf);
            buf.flip();
            
            String header = "";
            int r;
            char ch;
            
            r = buf.get();
            while(r != -1) {
                ch = (char) r;
                header += ch;
            }
            
            System.out.println(header);
            
            
            
        
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        
    }
        
        
    
}
